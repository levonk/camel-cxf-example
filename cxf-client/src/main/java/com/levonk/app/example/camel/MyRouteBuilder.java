package com.levonk.app.example.camel;

import org.apache.camel.builder.RouteBuilder;

/**
 * A Camel Java DSL Router
 */
public class MyRouteBuilder extends
		RouteBuilder
{

	private static final transient String SIMPLE_ENDPOINT_ADDRESS = "http://www.webservicex.net/globalweather.asmx/GetWeather";
	private static final transient String SIMPLE_ENDPOINT_URI = "cxf://" + MyRouteBuilder.SIMPLE_ENDPOINT_ADDRESS + "?"
			//+ "CityName=Burbank&CountryName=United+States&"
			+ "serviceClass=net.webservicex.GlobalWeather.class&"
			+ "publishedEndpointUrl=http://www.webservicex.net/globalweather.asmx";
	

	@Override
	public void configure()
	{

		// here is a sample which processes the input files
		// (leaving them in place - see the 'noop' flag)
		// then performs content based routing on the message using XPath
		this.from(MyRouteBuilder.SIMPLE_ENDPOINT_URI)
			.log("UK message")
			.to("file:target/messages/output");
	}
}
